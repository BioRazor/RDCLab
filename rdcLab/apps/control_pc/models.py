from django.db import models
from django.db.models.signals import post_save, pre_save

from .functions import generar_pdf_Desktop, generar_pdf_Laptop

class Servicio(models.Model):
	nombre = models.CharField(blank=False, max_length=50)

	def __str__(self):
		return(self.nombre)

	class Meta:
		verbose_name='Servicio'
		verbose_name_plural='Servicios'

class Servicio_Tecnico_Desktop(models.Model):
	cliente = models.ForeignKey('clientes.Cliente')
	tecnico = models.ForeignKey('tecnicos.Tecnico')
	pc = models.ForeignKey('pc.Desktop')

	servicios = models.ManyToManyField(Servicio)

	total = models.PositiveSmallIntegerField(blank=True)
	abono = models.PositiveSmallIntegerField(blank=True, null=True, default = 0)
	cancelado = models.BooleanField(blank=False)
	fecha_recepcion = models.DateField(auto_now_add=True)
	fecha_finalizacion = models.DateField(blank=True, null=True)
	fecha_entrega = models.DateField(blank=True, null=True)
	observaciones = models.TextField(blank=False, default='Ninguna')

	def __str__(self):
		return ('%s - %s - %s') %(self.cliente, self.tecnico, self.fecha_recepcion)


	class Meta:
		verbose_name='Servicio Tecnico PC'
		verbose_name_plural='Servicios Tecnicos PCs'


class Servicio_Tecnico_Laptop(models.Model):
	cliente = models.ForeignKey('clientes.Cliente')
	tecnico = models.ForeignKey('tecnicos.Tecnico')
	pc = models.ForeignKey('pc.Laptop')

	servicios = models.ManyToManyField(Servicio)

	total = models.PositiveSmallIntegerField(blank=True)
	abono = models.PositiveSmallIntegerField(blank=True, null=True, default=0)
	cancelado = models.BooleanField(blank=False)
	fecha_recepcion = models.DateField(auto_now_add=True)
	fecha_finalizacion = models.DateField(blank=True, null=True)
	fecha_entrega = models.DateField(blank=True, null=True)
	observaciones = models.TextField(blank=False, default='Ninguna')

	def __str__(self):
		return ('%s - %s - %s') %(self.cliente, self.tecnico, self.fecha_recepcion)

	class Meta:
		verbose_name='Servicio Tecnico Laptop'
		verbose_name_plural='Servicios Tecnicos Laptops' 


