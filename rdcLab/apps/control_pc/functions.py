from io import BytesIO
from django.http import HttpResponse
from django.template.loader import get_template
from xhtml2pdf import pisa

from apps.tecnicos.models import Tecnico
from apps.clientes.models import Cliente


template_laptop = 'pdfLaptop.html'
template_Desktop = 'pdfDesktop.html'

def render_to_pdf(template_src, context):
	template = get_template(template_src)
	context_dict = context
	html  = template.render(context_dict)
	result = BytesIO()
	pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
	if not pdf.err:
		response = HttpResponse(result.getvalue(), content_type='application/pdf')
		response['Content-Disposition'] = 'attachment; filename="%s.pdf"' %(context['nombre'])
		return response
	return None

#Funcion que prepara y envia los datos necesario para retornar el PDF de servicio
def generar_pdf_Desktop(instance):
	from apps.pc.models import Desktop
	context = {}
	context['servicio'] = instance
	context['tecnico'] = Tecnico.objects.get(id = instance.tecnico_id)
	context['cliente'] = Cliente.objects.get(id = instance.cliente_id)
	context['pc'] = Desktop.objects.get(id = instance.pc_id)
	context['nombre'] = context['cliente'].nombre + ' - ' + str(context['servicio'].fecha_recepcion) + ' - ' + context['tecnico'].nombre
	return render_to_pdf(template_Desktop, context)
	

def generar_pdf_Laptop(instance):
	from apps.pc.models import Laptop
	context = {}
	context['servicio'] = instance
	context['tecnico'] = Tecnico.objects.get(id = instance.tecnico_id)
	context['cliente'] = Cliente.objects.get(id = instance.cliente_id)
	context['laptop'] = Laptop.objects.get(id = instance.pc_id)
	context['nombre'] = context['cliente'].nombre + ' - ' + str(context['servicio'].fecha_recepcion) + ' - ' + context['tecnico'].nombre
	return render_to_pdf(template_laptop, context)