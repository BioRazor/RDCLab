# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-07 15:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('control_pc', '0003_auto_20171031_1158'),
    ]

    operations = [
        migrations.AlterField(
            model_name='servicio_tecnico_laptop',
            name='abono',
            field=models.PositiveSmallIntegerField(blank=True, default=0, null=True),
        ),
    ]
