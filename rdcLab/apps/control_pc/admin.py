from django.contrib import admin

from .models import *
from apps.tecnicos.models import Tecnico
from .functions import generar_pdf_Desktop

@admin.register(Servicio)
class ServicioAdmin(admin.ModelAdmin):
    list_display=('nombre',)
    search_fields=('nombre',)

@admin.register(Servicio_Tecnico_Desktop)
class Servicio_Tecnico_DesktopAdmin(admin.ModelAdmin):

	actions = ['getPDF']
	raw_id_fields=('cliente',)
	list_display = ['pc', 'tecnico', 'cliente', 'fecha_recepcion', 'fecha_finalizacion', 'fecha_entrega', 'button']
	filter_horizontal = ['servicios']
	exclude = ('tecnico',)

	def getPDF(self, request, queryset):
		for servicio in queryset:
			return generar_pdf_Desktop(servicio)
	getPDF.short_description = "Generar PDF's"

	def button(self, obj):
		link = u'<a class="button" href="/pdf_desktop/%s">PDF</a>' %(obj.id)
		return (link)
	button.short_description = 'PDF'
	button.allow_tags = True

	#al guardar, se asigna como tecnico, el usuario actual
	def save_model(self, request, obj, form, change):
		if not change:
			tecnico = Tecnico.objects.get(usuario__id = request.user.id)
			obj.tecnico_id = tecnico.id
		obj.save()

	#Si es super usuario puede seleccionar cualquier tecnico, vaciando la variable exclude
	def get_form(self, request, obj=None, **kwargs):
		if request.user.is_superuser:
			self.exclude = []
		return super(Servicio_Tecnico_DesktopAdmin, self).get_form(request, obj, **kwargs)

@admin.register(Servicio_Tecnico_Laptop)
class Servicio_TecnicoLaptopAdmin(admin.ModelAdmin):
	list_display = ['pc', 'tecnico', 'cliente', 'fecha_recepcion', 'fecha_finalizacion', 'fecha_entrega', 'finalizar', 'entregado', 'button']
	filter_horizontal = ['servicios']
	exclude=['tecnico']

	def button(self, obj):
		link = u'<a class="button" href="/pdf_laptop/%s">PDF</a>' %(obj.id)
		return (link)
	button.short_description = 'PDF'
	button.allow_tags = True

	def finalizar(self, obj):
		link = u'<a class="button" href="/finalizar/%s/%s">Finalizar</a>' %(obj.id, obj.pc.codigo)
		return (link)
	finalizar.short_description = 'Finalizar'
	finalizar.allow_tags = True

	def entregado(self, obj):
		link = u'<a class="button" href="/pdf_laptop/%s">Entregado</a>' %(obj.id)
		return (link)
	entregado.short_description = 'Entregado'
	entregado.allow_tags = True

		#al guardar, se asigna como tecnico, el usuario actual
	def save_model(self, request, obj, form, change):
		if not change:
			tecnico = Tecnico.objects.get(usuario__id = request.user.id)
			obj.tecnico_id = tecnico.id
		obj.save()

	#Si es super usuario puede seleccionar cualquier tecnico, vaciando la variable exclude
	def get_form(self, request, obj=None, **kwargs):
		if request.user.is_superuser:
			self.exclude = []
		return super(Servicio_TecnicoLaptopAdmin, self).get_form(request, obj, **kwargs)