import datetime

from django.shortcuts import render, get_object_or_404
from django.contrib import messages
from django.urls import reverse

from apps.control_pc.functions import generar_pdf_Desktop, generar_pdf_Laptop
from apps.control_pc.models import Servicio_Tecnico_Desktop, Servicio_Tecnico_Laptop

def generar_pdf_DesktopView(request, pk):
    instance = Servicio_Tecnico_Desktop.objects.get(pk = pk)
    return generar_pdf_Desktop(instance)

def generar_pdf_LaptopView(request, pk):
    instance = Servicio_Tecnico_Laptop.objects.get(pk = pk)
    return generar_pdf_Laptop(instance)

def finalizarView(request, pk, codigo):
    try:
        instance = get_object_or_404(Servicio_Tecnico_Desktop, pk = pk, pc__codigo = codigo)
        instance.fecha_finalizacion = datetime.datetime.now()
    except:
        instance = get_object_or_404(Servicio_Tecnico_Laptop, pk = pk, pc__codigo = codigo)
        instance.fecha_finalizacion = datetime.datetime.now()
        reverse('admin:Servicio_Tecnico_Laptop', current_app=request.resolver_match.namespace)
    else:
        return messages.error(request, 'Ningun servicio con ese id')