from django.contrib import admin

from apps.control_pc.models import Servicio_Tecnico_Desktop
from .models import Desktop, Ram, DD, CdRom, MoBo, Fuente, Cpu, Adicional, Laptop, Estado_PC, datos_Laptops

@admin.register(Estado_PC)
class EstadoPCADmin(admin.ModelAdmin):
    pass

@admin.register(Desktop)
class DesktopAdmin(admin.ModelAdmin):

	list_display = ['codigo', 'cliente', 'creado', 'observaciones']
	filter_horizontal = ['estado', 'ram', 'cdRom', 'adicional', 'dd',]
	exclude = ['codigo',]

@admin.register(Ram)
class RamAdmin(admin.ModelAdmin):
	pass

@admin.register(DD)
class DDAdmin(admin.ModelAdmin):
	pass

@admin.register(CdRom)
class CdRomAdmin(admin.ModelAdmin):
	pass

@admin.register(MoBo)
class MoBoAdmin(admin.ModelAdmin):
	pass

@admin.register(Fuente)
class FuenteAdmin(admin.ModelAdmin):
	pass

@admin.register(Cpu)
class CpuAdmin(admin.ModelAdmin):
	pass

@admin.register(Adicional)
class AdicionalAdmin(admin.ModelAdmin):
	pass

@admin.register(datos_Laptops)
class datos_LaptopsAdmin(admin.ModelAdmin):
	pass

@admin.register(Laptop)
class LaptopAdmin(admin.ModelAdmin):
	list_display = ['codigo', 'cliente', 'laptop', 'creado', 'observaciones']
	filter_horizontal = ['estado',]
	exclude = ['codigo',]
