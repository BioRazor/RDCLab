import random

def generar_codigo():
	return ''.join(random.choice('0123456789ABCDEFJKLMNOPQKRSTUVWXYZ') for i in range(4))