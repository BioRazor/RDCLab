from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Cliente(models.Model):
    """Clientes del Rey del Cassette."""

    usuario = models.OneToOneField(User, on_delete=models.CASCADE, blank=True, null=True)
    nombre = models.CharField(blank=False, null=False, max_length=50)
    cedula = models.CharField(blank=False, null=False, max_length=50, unique = True)
    prefijos= (
        ('0412','0412'),
        ('0426','0426'),
        ('0416','0416'),
        ('0424','0424'),
        ('0414','0414'),
        ('0246', '0246')
        )
    prefijo_telefono = models.CharField(blank=False, max_length=50, choices=prefijos)
    telefono = models.CharField(blank=False, max_length=7)
    creado = models.DateTimeField(auto_now_add=True)
    modificado = models.DateTimeField(auto_now=True)

    class Meta:
        """Meta definition for Cliente."""

        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'

    def __str__(self):
        """str representation of Cliente."""
        return ('%s - %s') %(self.nombre, self.cedula)
