from django.apps import AppConfig


class ControlMusicaConfig(AppConfig):
    name = 'control_musica'
