from django.db import models
from django.contrib.auth.models import User

class Tecnico(models.Model):
    usuario =  models.ForeignKey(User, on_delete=models.CASCADE, blank=False, null=False)
    nombre = models.CharField(blank=False, null=False, max_length=50)
    prefijos= (
        ('0412','0412'),
        ('0426','0426'),
        ('0416','0416'),
        ('0424','0424'),
        ('0414','0414'),
        ('0246', '0246')
        )
    prefijo_telefono = models.CharField(blank=False, max_length=50, choices=prefijos)
    telefono = models.CharField(blank=False, max_length=7)
    creado = models.DateTimeField(auto_now_add=True)
    modificado = models.DateTimeField(auto_now=True)

    class Meta:
        
        verbose_name = 'Tecnico'
        verbose_name_plural = 'Tecnicos'

    def __str__(self):
        return ('%s - %s - %s') %(self.nombre, self.usuario, self.telefono)
