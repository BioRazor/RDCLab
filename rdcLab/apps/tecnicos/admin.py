from django.contrib import admin
from .models import Tecnico

@admin.register(Tecnico)
class TecnicoAdmin(admin.ModelAdmin):
    list_display = ('nombre','usuario')
    search_fields = ('usuario','nombre')