"""rdcLab URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin


from apps.control_pc.views import generar_pdf_DesktopView, generar_pdf_LaptopView, finalizarView

urlpatterns = [
    url(r'^admin/', admin.site.urls),    
	url(r'^pdf_desktop/(?P<pk>\d+)/$', generar_pdf_DesktopView, name='pdfDesktop'),
	url(r'^pdf_laptop/(?P<pk>\d+)/$', generar_pdf_LaptopView, name='pdfDesktop'),
    url(r'^finalizar/(?P<pk>\d+)/(?P<codigo>\w{4})$', finalizarView, name='finalizarView'),
    url(r'^jet/', include('jet.urls', 'jet')),  # Django JET URLS
    url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),  # Django JET dashboard URLS
]
