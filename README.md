# RDCLab - Laboratorio de Aprendizaje

RDCLab, es lo que he llamado a lo que en un principio sera el campo de pruebas para aprendizaje a nivel profesional 
de Django y python. La Finalidad del sistema, es ofrecer todo el conjunto de soluciones posibles para la empresa
en un solo sitio.

## Se tiene planeado que el sistema se encargue de lo siguiente:

1.- Llevar el control de los Servicios Tecnicos realizados a las computadoras en la Empresa, registrando a los clientes,
tecnicos, computadoras y servicios realizados. 

2.- (Aun por decidir) Prestar las herramientas necesarias para automatizar el procesor de proyectos musicales solicitados
por clientes.

# Aún en desarrollo

Muchas cosas aun estan en desarrollo, aún queda por aprender, aportes, observaciones, sugerencias, criticas constructivas son bienvenidas